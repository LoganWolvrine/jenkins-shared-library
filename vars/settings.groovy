def call() {
 
  def setting = [:]

  //git configuration
  setting.accesstoken                       = "iUENiKyV6B5hPdoCxV3z"

  //private library configuration   
  setting.go_private_lib                    = "https://gitlab.com/dslt/codesmells.git"
  setting.go_private_lib_auth               = ""

  // jfrog cli
  setting.jfrog_cli                         = "https://getcli.jfrog.io"
  setting.jfrog_user                        = "admin"

  //tools configuration
  setting.artifactory_url                   = "https://klikdevsecops.jfrog.io"
  setting.container_registry                = "klikdevsecops.jfrog.io/klik-docker"
  setting.container_registry_url            = "https://klikdevsecops.jfrog.io/klik-docker"
  setting.registry_credential               = "jfrog-credential"
  setting.sonarqube_enterprise_url          = "https://sq.toolchain.klik.digital"
  setting.sonarqube_developer_url           = "https://sqd.toolchain.klik.digital"

  setting.recipient                         = "david.staliat@gmail.com"

  return setting
}
